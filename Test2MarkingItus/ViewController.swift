//
//  ViewController.swift
//  Test2MarkingItus
//
//  Created by Joseph John on 2019-11-07.
//  Copyright © 2019 Joseph John. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    @IBOutlet weak var startMonitoringButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var amountOfMohammadLabel: UILabel!
    @IBOutlet weak var timeSlowDownLabel: UILabel!
    @IBOutlet weak var amoundMohammadLabel: UILabel!
    
    
    let USERNAME = "josephjohn727@gmail.com"
    let PASSWORD = "Chanchal.27"
    
    
    let DEVICE_ID = "230032000f47363333343437"
    var myPhoton : ParticleDevice?
    
    var timer = Timer()
    var timerCounter = 0
    var sliderVisible = false
    var sliderValue=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.timerLabel.text = String(timerCounter)
        self.slider.isHidden=true
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
        
        
    }
    
   
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                
                self.subscribeToParticleEvents()
                //self.subscribeToXMotion()
            }
            
        }
    }
    
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    
                    //let choice = (event?.data)!
                    
                  
                }
        })
    }
    @IBAction func startMonitoringButtonPressed(_ sender: Any) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: Selector(("changeTimer")), userInfo: nil, repeats: true)
        self.slider.isHidden = false
        //self.sliderVisible = true
        if(timerCounter >= 11){
            self.sliderVisible=true
        }
        self.amoundMohammadLabel.text="Amount of Mohammad"
        self.timeSlowDownLabel.text="Time slows down by"
        
    }
    
    @IBAction func sliderBarMoved(_ sender: UISlider) {
         self.sliderValue = Int(sender.value)
        self.timeSlowDownLabel.text="Time slows down by:\(sliderValue)"
        print(sliderValue)
        if(timerCounter >= 11){
            timerCounter = timerCounter - sliderValue
        }
    }
    
    
    @objc func changeTimer(){
        timerCounter = timerCounter + 1
        self.timerLabel.text = String(timerCounter)
        
        if (timerCounter == 1){
            self.smileParticle(second: timerCounter)
        }else if(timerCounter == 5){
            self.smileParticle(second: timerCounter)
        }else if(timerCounter == 10){
            self.smileParticle(second: timerCounter)
        }else if(timerCounter == 15){
            self.smileParticle(second: timerCounter)
        }else if(timerCounter == 20){
            self.smileParticle(second: timerCounter)
            timer.invalidate()
        }
        
    }
    
    func smileParticle(second: Int) {
        
        let time = String(second)
        let parameters = [time]
        var task = myPhoton!.callFunction("changeFace", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error when sending message")
            }
        }
        
    }

}

