// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
InternetButton button = InternetButton();

void setup() {
    button.begin();
    
    Particle.function("changeFace",changeFace);
    
    
       for (int i = 0; i < 3; i++) {
    button.allLedsOn(0,20,0);
    delay(250);
    button.allLedsOff();
    delay(250);
  }

}

int changeFace(String cmd){
     int limit = cmd.toInt();

        
        if(limit == 1){
            button.ledOn(1,0,255,0);
        }else if (limit == 5){
            button.ledOn(1,0,255,0);
            button.ledOn(2,0,255,0);
            button.ledOn(3,0,255,0);
        } else if(limit == 10){
            button.ledOn(1,0,255,0);
            button.ledOn(2,0,255,0);
            button.ledOn(3,0,255,0);
            button.ledOn(4,255,255,0);
            button.ledOn(5,255,255,0);
            button.ledOn(6,255,255,0);
            
        }else if(limit == 15){
            button.ledOn(1,0,255,0);
            button.ledOn(2,0,255,0);
            button.ledOn(3,0,255,0);
            button.ledOn(4,255,255,0);
            button.ledOn(5,255,255,0);
            button.ledOn(6,255,255,0);
            button.ledOn(7,255,102,0);
            button.ledOn(8,255,102,0);
            button.ledOn(9,255,102,0);
            
        }else if (limit == 20){
            button.ledOn(1,255,0,0);
            button.ledOn(2,255,0,0);
            button.ledOn(3,255,0,0);
            button.ledOn(4,255,0,0);
            button.ledOn(5,255,0,0);
            button.ledOn(6,255,0,0);
            button.ledOn(7,255,0,0);
            button.ledOn(8,255,0,0);
            button.ledOn(9,255,0,0);
            button.ledOn(10,255,0,0);
            button.ledOn(11,255,0,0);
        }
        
        delay(5000);
        button.allLedsOff();
    }
    

void loop() {

}